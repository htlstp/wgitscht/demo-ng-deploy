import { Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {FooComponent} from "./foo/foo.component";

export const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'foo', component: FooComponent},
  {path: '', redirectTo: 'login', pathMatch: "full"}
];
