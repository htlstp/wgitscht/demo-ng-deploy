import { Component } from '@angular/core';
import {FormsModule} from "@angular/forms";
import {LoginService, User} from "../login.service";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    FormsModule,
    NgIf
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  user: User | undefined = undefined;

  constructor(private loginService: LoginService) {
  }

  token = ''

  getUser(){
    this.loginService.getUser(this.token).subscribe(
      (user) => {
          this.user = user;
      }
    )
  }
}
