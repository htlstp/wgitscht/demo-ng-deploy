import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {from, Observable} from "rxjs";

export  interface User {
  username: string,
  name: string
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private token = ''

  constructor(private httpCLient: HttpClient) { }

  getUser(token: string){
    this.token = token;
    let url = 'https://gitlab.com/api/v4/user'
    // url = '/user'
    return this.httpCLient.get<User>(url,
      {
        headers: {'PRIVATE-TOKEN':token}
      });
  }

}
